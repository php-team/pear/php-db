<?xml version="1.0" encoding="UTF-8"?>
<package packagerversion="1.10.9" version="2.0" xmlns="http://pear.php.net/dtd/package-2.0" xmlns:tasks="http://pear.php.net/dtd/tasks-1.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://pear.php.net/dtd/tasks-1.0 http://pear.php.net/dtd/tasks-1.0.xsd http://pear.php.net/dtd/package-2.0 http://pear.php.net/dtd/package-2.0.xsd">
 <name>DB</name>
 <channel>pear.php.net</channel>
 <summary>Database Abstraction Layer</summary>
 <description>DB is a database abstraction layer providing:
* an OO-style query API
* portability features that make programs written for one DBMS work with other DBMS&apos;s
* a DSN (data source name) format for specifying database servers
* prepare/execute (bind) emulation for databases that don&apos;t support it natively
* a result object for each query response
* portable error codes
* sequence emulation
* sequential and non-sequential row fetching as well as bulk fetching
* formats fetched rows as associative arrays, ordered arrays or objects
* row limit support
* transactions support
* table information interface
* DocBook and phpDocumentor API documentation

DB layers itself on top of PHP&apos;s existing
database extensions.

Drivers for the following extensions pass
the complete test suite and provide
interchangeability when all of DB&apos;s
portability options are enabled:

  fbsql, ibase, informix, msql, mssql,
  mysql, mysqli, oci8, odbc, pgsql,
  sqlite and sybase.

There is also a driver for the dbase
extension, but it can&apos;t be used
interchangeably because dbase doesn&apos;t
support many standard DBMS features.

DB is compatible with PHP 5 and PHP 8.</description>
 <lead>
  <name>Daniel Convissor</name>
  <user>danielc</user>
  <email>danielc@php.net</email>
  <active>no</active>
 </lead>
 <lead>
  <name>Adam Harvey</name>
  <user>aharvey</user>
  <email>aharvey@php.net</email>
  <active>no</active>
 </lead>
 <lead>
  <name>Armin Graefe</name>
  <user>schengawegga</user>
  <email>schengawegga@gmail.com</email>
  <active>yes</active>
 </lead>
 <developer>
  <name>Stig Bakken</name>
  <user>ssb</user>
  <email>stig@php.net</email>
  <active>no</active>
 </developer>
 <developer>
  <name>Tomas V.V.Cox</name>
  <user>cox</user>
  <email>cox@idecnet.com</email>
  <active>no</active>
 </developer>
 <date>2024-04-15</date>
 <time>21:14:49</time>
 <version>
  <release>1.12.2</release>
  <api>1.12.0</api>
 </version>
 <stability>
  <release>stable</release>
  <api>stable</api>
 </stability>
 <license uri="http://www.php.net/license">PHP License</license>
 <notes>
* Task: Manage E_DEPRECATED #27
* Task: Remove CVS id that no longer makes sense #30
* Bug: Bug in DB/oci8.php ... oci_fetch_array called with wrong parameters #33 #34
* Bug: Errors raised in DB_storage::toString() if there are multi-column keys, on PHP &gt;= 7 #35
* Bug: sqlite3 back-end incorrectly refers to &apos;resource&apos; objects #38 #39
 </notes>
 <contents>
  <dir name="/">
   <file md5sum="2544db05bc99ae46e61392f56d35df21" name="DB/common.php" role="php">
    <tasks:replace from="@package_version@" to="version" type="package-info" />
   </file>
   <file md5sum="7b2ff6b9123aca7c804d502604b86f2f" name="DB/dbase.php" role="php">
    <tasks:replace from="@package_version@" to="version" type="package-info" />
   </file>
   <file md5sum="6b5de4e590bfee3e78255accdb246b75" name="DB/fbsql.php" role="php">
    <tasks:replace from="@package_version@" to="version" type="package-info" />
   </file>
   <file md5sum="db122d99133bf818402b2b91cea9fa7c" name="DB/ibase.php" role="php">
    <tasks:replace from="@package_version@" to="version" type="package-info" />
   </file>
   <file md5sum="4b5fade0c53b402ed22ae194acfbdcb1" name="DB/ifx.php" role="php">
    <tasks:replace from="@package_version@" to="version" type="package-info" />
   </file>
   <file md5sum="0160195a59af3f6aa93ad4d86b296659" name="DB/msql.php" role="php">
    <tasks:replace from="@package_version@" to="version" type="package-info" />
   </file>
   <file md5sum="b883524a723fa195f598204a5824f02a" name="DB/mssql.php" role="php">
    <tasks:replace from="@package_version@" to="version" type="package-info" />
   </file>
   <file md5sum="95391d68b402a4580d8b46d2ba084d49" name="DB/mysql.php" role="php">
    <tasks:replace from="@package_version@" to="version" type="package-info" />
   </file>
   <file md5sum="38c42d71991369cf7c57954eab26355e" name="DB/mysqli.php" role="php">
    <tasks:replace from="@package_version@" to="version" type="package-info" />
   </file>
   <file md5sum="392112a520799df8e00160d9637585b8" name="DB/oci8.php" role="php">
    <tasks:replace from="@package_version@" to="version" type="package-info" />
   </file>
   <file md5sum="223b4b073c98c1de14caf97d246bea81" name="DB/odbc.php" role="php">
    <tasks:replace from="@package_version@" to="version" type="package-info" />
   </file>
   <file md5sum="cdb972441c5425d5c5ccfad26dd7f595" name="DB/pgsql.php" role="php">
    <tasks:replace from="@package_version@" to="version" type="package-info" />
   </file>
   <file md5sum="43e59d486688f8ad5b4f936fdf91341a" name="DB/sqlite.php" role="php">
    <tasks:replace from="@package_version@" to="version" type="package-info" />
   </file>
   <file md5sum="f332836fe7a0237ad9ec83a032d2da5b" name="DB/sqlite3.php" role="php">
    <tasks:replace from="@package_version@" to="version" type="package-info" />
   </file>
   <file md5sum="d7f1fbfff83e236a580607d049b9daa5" name="DB/storage.php" role="php">
    <tasks:replace from="@package_version@" to="version" type="package-info" />
   </file>
   <file md5sum="6cf34a1086f02b8a3ccd709ae9673ec4" name="DB/sybase.php" role="php">
    <tasks:replace from="@package_version@" to="version" type="package-info" />
   </file>
   <file md5sum="c32c12da907325fa75b1d7750599438b" name="doc/IDEAS" role="doc" />
   <file md5sum="a53305e9d7db39962e668b5dd0e8a74e" name="doc/MAINTAINERS" role="doc" />
   <file md5sum="ebe7f34974fd913adc8c0e65e3171257" name="doc/STATUS" role="doc" />
   <file md5sum="d9c5b209a61d4836e4d2d61736aa7d48" name="doc/TESTERS" role="doc" />
   <file md5sum="f04a27ed9965c9a92b3da8f3f9471d2a" name="tests/driver/01connect.phpt" role="test" />
   <file md5sum="e5e6bbc5d08a780a4adc147e140b7c66" name="tests/driver/02fetch.phpt" role="test" />
   <file md5sum="10e0a56a8463bdbf21535475ab976367" name="tests/driver/03simplequery.phpt" role="test" />
   <file md5sum="428f16df464e6155f9c61acaf005217e" name="tests/driver/04numcols.phpt" role="test" />
   <file md5sum="023630fcbc8e0a506f5cfa0965b0e561" name="tests/driver/05sequences.phpt" role="test" />
   <file md5sum="802be3b6eb61644276792ece7fe20da1" name="tests/driver/06prepexec.phpt" role="test" />
   <file md5sum="1c531bec2030b886a152c8ef0989dc45" name="tests/driver/08affectedrows.phpt" role="test" />
   <file md5sum="3d7004c26a88ff87c89d891ce402432e" name="tests/driver/09numrows.phpt" role="test" />
   <file md5sum="088612c74e59c40b90a3ac0a524d97ca" name="tests/driver/10errormap.phpt" role="test" />
   <file md5sum="9cf9ee017f2aeb2137d6a4ab75a0a87b" name="tests/driver/11transactions.phpt" role="test" />
   <file md5sum="3c5589b4a79326e155351ff6323e04a5" name="tests/driver/13limit.phpt" role="test" />
   <file md5sum="9f266b2ab7e55475cfdb50f2020bb3f5" name="tests/driver/14fetchmode_object.phpt" role="test" />
   <file md5sum="9a65edab760691e63df2a5ba945b33b8" name="tests/driver/15quote.phpt" role="test" />
   <file md5sum="c8e45620f7bcdb113ec22d4863c5c655" name="tests/driver/16tableinfo.phpt" role="test" />
   <file md5sum="4014b9fbe1bc9f78c10ea10d756678f9" name="tests/driver/17query.phpt" role="test" />
   <file md5sum="f9707e12185a3a48ea940d003848ffa5" name="tests/driver/18get.phpt" role="test" />
   <file md5sum="1a28f48f074902dc046a44b69f18bc93" name="tests/driver/19getlistof.phpt" role="test" />
   <file md5sum="dd6ca94f8e0a57c1706778762f386e95" name="tests/driver/20locale.phpt" role="test" />
   <file md5sum="f01f57022d9aba072bef0ec8103005db" name="tests/driver/21freeResult.phpt" role="test" />
   <file md5sum="bb70169e5397d4e6da16e9648ac55f0f" name="tests/driver/connect.inc" role="test" />
   <file md5sum="135782ce851c1cdb9f2c2d4a87a6ed60" name="tests/driver/droptable.inc" role="test" />
   <file md5sum="fce3c26483112059877bc4de99beec85" name="tests/driver/mktable.inc" role="test" />
   <file md5sum="2fa5a2425834f6767d026786e09d74b2" name="tests/driver/multiconnect.php" role="test" />
   <file md5sum="c1e9f81b080bf59cf0279726a06efd5a" name="tests/driver/run.cvs" role="test" />
   <file md5sum="b7fa1910208b55b354ad03be6995bd6b" name="tests/driver/setup.inc" role="test">
    <tasks:replace from="@include_path@" to="php_dir" type="pear-config" />
   </file>
   <file md5sum="baefe117267233e4db3d5d1a01e8ed3d" name="tests/driver/skipif.inc" role="test" />
   <file md5sum="469f144cfa33258cf89d84263930cec5" name="tests/db_error.phpt" role="test" />
   <file md5sum="61ff495f443383aeb1925af96f713629" name="tests/db_error2.phpt" role="test" />
   <file md5sum="deade1e2315dfbd2277de45ea56992e7" name="tests/db_factory.phpt" role="test" />
   <file md5sum="94774adc51c7ef307e9f4b12d030f0b1" name="tests/db_ismanip.phpt" role="test" />
   <file md5sum="4187ebe3ea3c3cb35b52bc0475ad0ff0" name="tests/db_parsedsn.phpt" role="test" />
   <file md5sum="770ff645671e7135430a0f22b69e6304" name="tests/errors.inc" role="test" />
   <file md5sum="ce3cb029328e3a848d128c01b4e9e051" name="tests/fetchmodes.inc" role="test" />
   <file md5sum="83c2fa8d2a864d15f98d870c88966fa6" name="tests/fetchmode_object.inc" role="test" />
   <file md5sum="89abd1a426a593ca5bc7cf92a1ea10cb" name="tests/include.inc" role="test">
    <tasks:replace from="@include_path@" to="php_dir" type="pear-config" />
   </file>
   <file md5sum="92a8178cd964cfc294d91b35dc3b9b7c" name="tests/limit.inc" role="test" />
   <file md5sum="7751bf5bf888f5977840163e7f752863" name="tests/numcols.inc" role="test" />
   <file md5sum="669e7edf19916f1baa7d2cad4299c31a" name="tests/numrows.inc" role="test" />
   <file md5sum="595317b5a9ba04a557aeea15ea3f37b8" name="tests/prepexe.inc" role="test" />
   <file md5sum="d6eb09c8e4c148ed8e2b90704faba353" name="tests/run.cvs" role="test" />
   <file md5sum="ac7a29167291e53ccf91e3f74c7b4b74" name="tests/sequences.inc" role="test" />
   <file md5sum="2b94395033dd97768bae1f7ce5f6cfad" name="tests/simplequery.inc" role="test" />
   <file md5sum="51493bc4c8b4a76a81df2bb2a05b8ad7" name="tests/skipif.inc" role="test" />
   <file md5sum="d9cf4cfa24d67427621a92f273a56afc" name="tests/transactions.inc" role="test" />
   <file baseinstalldir="/" md5sum="7618f35d908fd148aa33f0836487d2a3" name="DB.php" role="php">
    <tasks:replace from="@package_version@" to="version" type="package-info" />
   </file>
  </dir>
 </contents>
 <dependencies>
  <required>
   <php>
    <min>5.0.0</min>
   </php>
   <pearinstaller>
    <min>1.4.0b1</min>
   </pearinstaller>
   <package>
    <name>PEAR</name>
    <channel>pear.php.net</channel>
    <min>1.10.0</min>
   </package>
  </required>
 </dependencies>
 <phprelease />
</package>
